#include <algorithm>
#include <experimental/filesystem>

#include "mzip2.h"

namespace fs = std::experimental::filesystem::v1;

using namespace mzip2;

namespace {

// add forward slash if prefix is not empty
std::string make_canonical_prefix(const std::string &prefix)
{
    return !prefix.empty() ? prefix + (prefix.back() != '/' ? "/" : "") : "";
}

// remove leading forward/back slash and replace back slashes with forward
std::string normalize_path(const std::string &path)
{
    std::string result{path.front() == '/' || path.front() == '\\' ? path.data() + 1 : path.data()};    
    std::replace(result.begin(), result.end(), '\\', '/');    
    return result;                   
}

} // namespace

writer::writer(const std::string &archive)
{
    memset(&zip, 0, sizeof(zip));
    if (!mz_zip_writer_init_file(&zip, archive.c_str(), 0))    
        throw error(mz_zip_get_error_string(zip.m_last_error));
}

writer::~writer()
{
    // ignoring errors here, we are in destructor...
    mz_zip_writer_finalize_archive(&zip);
    mz_zip_writer_end(&zip);
}

void writer::add_path(const std::string &path)
{
    const std::string canonical_path = normalize_path(make_canonical_prefix(path));
                                       
    if (!mz_zip_writer_add_mem(&zip, canonical_path.c_str(), nullptr, 0, MZ_BEST_SPEED))
        throw error(mz_zip_get_error_string(zip.m_last_error));    
}

void writer::add_file(const std::string &source, const std::string &prefix)
{                
    const fs::path source_path(source);

    if (!fs::is_regular_file(source_path) || !fs::exists(source_path))
        throw error("Input argument '" + source + "' is not an existing file");
    
    const std::string archive_name = normalize_path(make_canonical_prefix(prefix) + source_path.filename().string());

    if (!mz_zip_writer_add_file(&zip, archive_name.c_str(), source.c_str(), nullptr, 0, MZ_BEST_SPEED))
        throw error(mz_zip_get_error_string(zip.m_last_error));        
}

void writer::add_dir_recursively(const std::string &source, const std::string &prefix)
{
    const fs::path source_path(source);

    if (!fs::is_directory(source_path) || !fs::exists(source_path))
        throw error("Input argument '" + source + "' is not an existing directory");

    const std::string dir_name_in_archive = make_canonical_prefix(prefix) + source_path.filename().string();
    
    add_path(dir_name_in_archive);
    
    for (const auto &it : fs::directory_iterator(source_path)) {
        if (fs::is_directory(it))
            add_dir_recursively(it.path().string(), dir_name_in_archive);
        else if (fs::is_regular_file(it))
            add_file(it.path().string(), dir_name_in_archive);
    }
}
