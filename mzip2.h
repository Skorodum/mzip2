#pragma once

#include <stdexcept>

#include "miniz.h"

namespace mzip2 {

class error : public std::runtime_error {
public:
    explicit error(const std::string &what) : std::runtime_error(what) {}
};

class writer
{
    mz_zip_archive zip;
    
public:
    explicit writer(const std::string &archive);
    ~writer();
    void add_file(const std::string &source, const std::string &prefix = {});
    void add_path(const std::string &path);
    void add_dir_recursively(const std::string &source, const std::string &prefix = {});
};

}
