import qbs

Product {
    name: "mzip2"
    type: "staticlibrary"
    
    Depends { name: "cpp" }

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: product.sourceDirectory            
    }        
       
    cpp.defines: "MINIZ_NO_TIME"
    
    files: [
        "mzip2.h",
        "mzip2.cpp",
        "miniz.h",
        "miniz.c",
    ]
}
